import * as postController from '../controllers/post.controllers.js';
import { Router } from 'express';

const router = Router();

router.get('/', postController.getAllPosts);
router.get('/:id', postController.getPostById);
router.post('/', postController.createPost);
router.patch('/:id', postController.modifyPost);
router.delete('/:id', postController.deletePost);

export default router;