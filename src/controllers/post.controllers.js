import Posts from '../models/Post.js';

export const getAllPosts = async (req, res) =>{
    try {
        const posts = await Posts.findAll();
        res.json({
            data: posts
        });
        
    } catch (error) {
        console.log(error)        
    }
};

export const getPostById = async (req, res) => {
    const { id } = req.params;
    const post = await Posts.findOne({
        where: {
            id
        }
    });
    res.json(post)
}

export const createPost = async (req, res) => {
    const { title, content, imageUrl, category, creationDate } = req.body;
    try {
        let newPost = await Posts.create({
            title,
            content,
            imageUrl,
            category,
            creationDate
        }, {
            fields: ['title', 'content', 'imageUrl', 'category', 'creationDate']
        });
        if (newPost) {
            return res.json({
                message: 'Post created succesfully',
                data: newPost
            });
        }       
    } catch (error) {
        console.log(error)
    }

    
}

export const modifyPost = async (req, res) => {
    const { id } = req.params;
    const { title, content, imageUrl, category, creationDate } = req.body;
    try {
        const posts = await Posts.findAll({
            attributes: ['id', 'title', 'content', 'imageUrl', 'category', 'creationDate'],
            where: { id }
        });
        if (posts.length > 0) {
            posts.forEach(async posts => {
                await posts.update({
                    title,
                    content,
                    imageUrl,
                    category,
                    creationDate
                });
            });
        }
        return res.json({
            message: 'Post updated successfully',
            posts
        });
        
    } catch (error) {
        console.log(error);
    }
}

export const deletePost = async (req, res) => {
    try {
        const { id } = req.params;
        const post = await Posts.destroy({
            where: {
                id
            }
        });
        res.json({
            message : "Post deleted succesfully"
        });
        
    } catch (error) {
        console.log(error)
    }
}