import Sequelize from 'sequelize';
import { sequelize } from '../database.js';

const Posts = sequelize.define('posts', {
    id: {
        type: Sequelize.INTEGER,
        primaryKey: true,
        autoIncrement: true,
    },
    title: {
        type: Sequelize.STRING
    },
    content: {
        type: Sequelize.TEXT
    },
    imageUrl: {
        type: Sequelize.TEXT
    },
    category: {
        type: Sequelize.STRING
    },
    creationDate: {
        type: Sequelize.DATE,
    }
    
}, {
    timestampes: false
});

export default Posts;