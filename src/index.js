import express, { json } from 'express';
import morgan from 'morgan';
import bodyParser from 'body-parser';
import postsRoutes from './routes/post.routes.js';
import cors from 'cors';

//settings
const app = express();
app.set('port', process.env.PORT || 3000);
app.listen(app.get('port'), () => {
    console.log('Server on port 3000');
});

//middleware
app.use(morgan('dev'));
app.use(bodyParser.json());
app.use(cors());


//routes
app.use('/api/posts', postsRoutes);

export default app;